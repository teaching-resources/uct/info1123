// Polymorphism example
// 2015-10-01

package Polymorphism;

public class Polymorphism {

    public static void main(String[] args) {

        Animal animal = new Animal(4);
        Animal dog = new Dog(5);
        Animal cat = new Cat(6);
        animal.move();
        dog.move();
        cat.move();
        dog.bodyParts();
        cat.bodyParts();

    }
}

class Animal {

    public int legs;

    public Animal(int legs) {
        this.legs = legs;
    }

    public void bodyParts() {
    }

    public void move() {
        System.out.println("Animals can move");
    }
}

class Dog extends Animal {

    public Dog(int legs) {
        super(legs);
    }

    public String ears = "Dogs have rounded ears";

    public void bodyParts() {
        System.out.println("Dogs have " + legs + " legs");
        System.out.println(ears);
    }

    public void move() {
        System.out.println("Dogs can walk and run");
    }
}

class Cat extends Animal {

    public Cat(int legs) {
        super(legs);
    }

    public String ears = "Cats have pointed ears";

    public void bodyParts() {
        System.out.println("Dogs have " + legs + " legs");
        System.out.println(ears);
    }

    public void move() {
        System.out.println("Cats can walk");
    }
}