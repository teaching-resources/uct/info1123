
# Polimorfismo: la habilidad de un objeto de tomar varias formas

# En el caso de programacion orientada a objetos, la habilidad de un objeto de tomar varias formas
# En el caso de programacion funcional, la habilidad de una funcion de tomar varias formas

# Operador aritmetico
print(1+1)  # 2

# Operador de concatenado
print("uno" + "uno")  # "unouno"

# Operador combinado
print(1 + "uno")  # TypeError: unsupported operand type(s) for +: 'int' and 'str'

# El operador + puede tomar varias formas, dependiendo del tipo de dato que se le pase
# En el caso de los numeros, el operador + hace una suma
# En el caso de los strings, el operador + hace una concatenacion
