from dataclasses import dataclass


@dataclass
class Transporte:
    tipo_carga: str
    masa: str
    tonelaje: int


@dataclass
class Contenedor(Transporte):

    def __post_init__(self):
        if self.tonelaje <= 12000:
            self.tamanho = "pequeño"
        else:
            self.tamanho = "grande"


x = Contenedor("normal", "liquida", 10000)
print(x.tipo_carga)
print(x.masa)
print(x.tonelaje)
print(x.tamanho)

print()

y = Contenedor("normal", "liquida", 20000)
print(y.tipo_carga)
print(y.masa)
print(y.tonelaje)
print(y.tamanho)
