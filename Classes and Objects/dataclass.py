from dataclasses import dataclass


@dataclass
class Car:
    name: str = "General"
    make: str = "General"
    year: int = 2020
    vehicle_type: str = "Default"


car = Car("Jazz", "Honda", 2008, "Hatch")
print(car)
print(car.name)
print(car.make)
print(car.year)
print(car == Car("Jazz", "Honda", 2008, "Hatch"))

default_car = Car()
print(default_car)
