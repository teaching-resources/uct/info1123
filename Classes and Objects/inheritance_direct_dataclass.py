from dataclasses import dataclass


@dataclass
class Person:
    firstname: str = "No First Name"
    lastname: str = "No Last Name"

    # __init__ override to receive outside arguments
    def __init__(self, fname, lname):
        self.firstname = fname
        self.lastname = lname

    def printname(self) -> str:
        print(self.firstname, self.lastname)

# Use the Person class to create an object, and then execute the printname method:


x = Person("John", "Doe")
x.printname()


class Student(Person):
    pass


x = Student("Mike", "Olsen")
x.printname()
